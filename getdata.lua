local ie = _G
if minetest.request_insecure_environment then
    ie = minetest.request_insecure_environment()
end

local path = minetest.get_modpath("getdata")

if ie then
    ie.os.execute('date +"%Y-%m-%dT%H:%M:%SZ"')
    local state = os.execute(command.." > "..path.."/output")
    local f = io.open(path.."/output", "r")
    os.execute("rm "..path.."/output")
else
    -- complain
end


local http = minetest.request_http_api()
assert(http)
http.fetch({
        url = "http://localhost:3300/mk_weather"
}, function(res)
        print(dump(res))
end)

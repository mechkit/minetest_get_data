


minetest.register_node("getdata:httpget", {
  drawtype = "nodebox",
  tiles = {"getdata_client_side.png", "getdata_client_side.png", "getdata_client_side.png", "getdata_client_side.png", "getdata_client_side.png", "getdata_client_off.png"},
  paramtype = "light",
  paramtype2 = "facedir",
  groups = {dig_immediate=2, not_in_creative_inventory=1},
  description="httpget",
  on_construct = function(pos)
    local meta = minetest.env:get_meta(pos)
    meta:set_string("infotext", "")
  end,
  on_rightclick = function (pos)
    minetest.sound_play("getdata_on",{pos=pos,gain=0.7,max_hear_distance=32})
    local facing = minetest.env:get_node(pos).param2
    minetest.env:set_node(pos, { name="getdata:httpget_on", param2=facing })
  end,
  sounds = default.node_sound_wood_defaults(),
})

minetest.register_node("getdata:httpget_on", {
  drawtype = "nodebox",
  tiles = {"getdata_client_side.png", "getdata_client_side.png", "getdata_client_side.png", "getdata_client_side.png", "getdata_client_side.png", "getdata_client.png"},
  paramtype = "light",
  paramtype2 = "facedir",
  light_source = 3,
  groups = {not_in_creative_inventory=1},
  description = "httpget",
  on_construct = function(pos)
    local meta = minetest.env:get_meta(pos)
    meta:set_string("formspec", "field[text;;${command}]")
    meta:set_string("channel-in", "keyboard")
    meta:set_string("channel-out", "getdata")
    meta:set_string("command", "")
  end,
  on_receive_fields = function(pos, formname, fields, sender)
    local privs = minetest.get_player_privs(sender:get_player_name())
    if ( privs == nil or not privs["getdata"] ) then return "You do not have getdata privs" end
    local meta = minetest.get_meta(pos)
    local command = fields.text
    meta:set_string("command",  command)
    local player = sender:get_player_name()
    local getdata_output = getdata_command(fields.text, player, pos)
    if (getdata_output == "exit") then
      local facing = minetest.env:get_node(pos).param2
      minetest.env:set_node(pos, { name="getdata:httpget", param2=facing })
    else
      meta:set_string("infotext", command)
      minetest.chat_send_player(player, getdata_output, false)
    end
  end,
  on_punch = function (pos, node, puncher)
    local privs = minetest.get_player_privs(puncher:get_player_name())
    if ( privs == nil or not privs["getdata"] ) then return "You do not have getdata privs" end
    local meta = minetest.env:get_meta(pos)
    local command = meta:get_string("command")
    local player = puncher:get_player_name()
    local getdata_output = getdata_command(command, player, pos)
    minetest.chat_send_player(player, getdata_output, false)
  end,
  mesecons = { effector = {
    action_on = function (pos, node)
      local meta = minetest.env:get_meta(pos)
      local command = meta:get_string("command")
      getdata_command(command, "mesecons", pos)
    end,
  }},
  digiline = { receptor = {},
    effector = {
      action = function(pos, node, channel, msg)
        local meta = minetest.env:get_meta(pos)
        local setchan = meta:get_string("channel-in")
        if setchan ~= channel then return end
        local command = meta:get_string("command")
        getdata_command(msg, "digilines", pos)
      end
    },
  },
})

function getdata_command(command, sender, pos)
  local privs = minetest.get_player_privs(sender)
  if (sender == "mesecons" or sender == "digilines") then
    if (getdata.allow_mesecons_and_digilines == false) then
      print("[getdata] see getdata/init.lua to enable mesecon and digiline triggering")
      local meta = minetest.env:get_meta(pos)
      local channel = meta:get_string("channel-out")
      digiline:receptor_send(pos, digiline.rules.default, channel, "see init.lua to enable digilines and mesecons") --next version will also send player threw digilines (will require a table)
      return
    end
  else
    if ( privs == nil or not privs["getdata"] ) then return "You do not have getdata privs" end
  end
  print(sender.." executed \""..command.."\" on client at "..minetest.pos_to_string(pos))
  if (command == "exit" or command == "logout") then
    return "exit"
  end
  if (string.sub(command, 1,13) == "digilines -in") then
    local meta = minetest.env:get_meta(pos)
    local channel = string.sub(command,15)
    meta:set_string("channel-in", channel)
    return "> "..command.."\n getdata channel input has been renamed to "..channel
  end
  if (string.sub(command, 1,14) == "digilines -out") then
    local meta = minetest.env:get_meta(pos)
    local channel = string.sub(command,16)
    meta:set_string("channel-out", channel)
    return "> "..command.."\n getdata channel out has been renamed to "..channel
  end

  assert(http)
  http.fetch({
    url = "http://localhost:3300/mk_weather"
  }, function(res)
    minetest.log(dump(res))
    minetest.log(res.data)

    if (res.succeeded) then
      print('succeeded')
      local meta = minetest.env:get_meta(pos)
      local channel = meta:get_string("channel-out")
      minetest.log(type(res.data))
      local data = minetest.parse_json(res.data)

      local msg = data.date..'\n'..data.time..'\n'..string.format("%.1f", data.irr)

      digiline:receptor_send(pos, digiline.rules.default, channel, msg)

      minetest.set_timeofday( data.day_fraction )

    else
      print(dump(res))
    end

  end)
  minetest.sound_play("getdata_on",{pos=pos,gain=0.7,max_hear_distance=32})
  return "irr set"
end

local modpath = minetest.get_modpath("getdata")

local ie = _G
if minetest.request_insecure_environment then
    ie = minetest.request_insecure_environment()
end

http = minetest.request_http_api()

minetest.register_privilege("getdata", "Can use getdata nodes")

getdata = {}
--set to true of you want to use mesecons and digilines triggering
getdata.allow_mesecons_and_digilines = false


technic = rawget(_G, "technic") or {}

-- Boilerplate to support intllib
if rawget(_G, "intllib") then
	technic.getter = intllib.Getter()
else
	technic.getter = function(s,a,...)if a==nil then return s end a={a,...}return s:gsub("(@?)@(%(?)(%d+)(%)?)",function(e,o,n,c)if e==""then return a[tonumber(n)]..(o==""and c or"")else return"@"..o..n..c end end) end
end
local S = technic.getter

local storage = minetest.get_mod_storage()

-- Load files
dofile(modpath.."/lib/init.lua")
dofile(modpath.."/client.lua")
dofile(modpath.."/httpget.lua")
dofile(modpath.."/nodes.lua")
-- dofile(modpath.."/register/solar_array.lua")
-- dofile(modpath.."/solar_array.lua")
